import { createAction } from 'typesafe-actions';

const CREATE_ACCOUNT = 'register/CREATE_ACCOUNT';
const CREATE_ACCOUNT_SUCCESS = 'register/CREATE_ACCOUNT_SUCCESS';
const CREATE_ACCOUNT_FAILURE = 'register/CREATE_ACCOUNT_FAILURE';

const GENERATE_MNEMONIC = 'register/GENERATE_MNEMONIC';
const GENERATE_MNEMONIC_SUCCESS = 'register/GENERATE_MNEMONIC_SUCCESS';
const GENERATE_MNEMONIC_FAILURE = 'register/GENERATE_MNEMONIC_FAILURE';

const RESET = 'register/RESET';
const RESET_SUCCESS = 'register/RESET_SUCCESS';

export const createAccountAction = createAction(CREATE_ACCOUNT, resolve => (mnemonic: string, password: string) =>
  resolve({ mnemonic, password })
);

export const createAccountSuccessAction = createAction(
  CREATE_ACCOUNT_SUCCESS,
  resolve => (account: Account, successMessage) => resolve({ account }, { successMessage })
);
export const createAccountFailureAction = createAction(CREATE_ACCOUNT_FAILURE, resolve => (error: Error) =>
  resolve(error.message)
);

export const generateMnemonicAction = createAction(GENERATE_MNEMONIC);
export const generateMnemonicSuccessAction = createAction(GENERATE_MNEMONIC_SUCCESS, resolve => mnemonic =>
  resolve({ mnemonic })
);
export const generateMnemonicFailureAction = createAction(GENERATE_MNEMONIC_FAILURE, resolve => (error: Error) =>
  resolve(error)
);

export const resetAction = createAction(RESET);
export const resetSuccessAction = createAction(RESET_SUCCESS);
