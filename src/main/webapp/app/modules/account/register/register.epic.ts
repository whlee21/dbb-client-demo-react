import { ActionType, isActionOf } from 'typesafe-actions';
import { Epic } from 'redux-observable';
import { map, filter, mergeMap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { translate } from 'react-jhipster';

import * as actions from './register.action';
import { RegisterState } from './register.reducer';
import { dbbClientService } from 'app/shared/service/dbb-client.service';

type Action = ActionType<typeof actions>;

const registerEpic: Epic<Action, Action, RegisterState> = (action$, state$) =>
  action$.pipe(
    filter(isActionOf(actions.createAccountAction)),
    mergeMap(action =>
      dbbClientService.registration(action.payload.mnemonic, action.payload.password).pipe(
        map(response => actions.createAccountSuccessAction(response, translate('register.messages.success'))),
        catchError(error => of(actions.createAccountFailureAction(error)))
      )
    )
  );

const generateMnemonicEpic: Epic<Action, Action, RegisterState> = (action$, state$) =>
  action$.pipe(
    filter(isActionOf(actions.generateMnemonicAction)),
    mergeMap(() =>
      dbbClientService.getNewMnemonic().pipe(
        map(mnemonic => actions.generateMnemonicSuccessAction(mnemonic)),
        catchError(error => of(actions.generateMnemonicFailureAction(error)))
      )
    )
  );

const resetEpic: Epic<Action, Action, RegisterState> = (action$, state$) =>
  action$.pipe(
    filter(isActionOf(actions.resetAction)),
    map(() => {
      dbbClientService.clearAccount();
      return actions.resetSuccessAction();
    })
  );

export default [registerEpic, generateMnemonicEpic, resetEpic];
