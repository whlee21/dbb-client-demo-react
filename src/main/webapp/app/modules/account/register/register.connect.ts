import { ActionType } from 'typesafe-actions';
import { Dispatch, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';

import * as actions from './register.action';
import RegisterPage from './register';
import { IRootState } from 'app/shared/redux/reducers';

type Action = ActionType<typeof actions>;

export interface IRegisterProps extends StateProps, DispatchProps, RouteComponentProps<{}> {
  handleRegister: Function;
  handleGenerate: Function;
  reset: Function;
}

const mapStateToProps = ({ locale, register }: IRootState) => ({
  currentLocale: locale.currentLocale,
  mnemonic: register.mnemonic
});

const mapDispatchToProps = (dispatch: Dispatch<Action>, props: IRegisterProps) =>
  bindActionCreators(
    {
      handleRegister: (mnemonic, password) => actions.createAccountAction(mnemonic, password),
      handleGenerate: () => actions.generateMnemonicAction(),
      reset: () => actions.resetAction()
    },
    dispatch
  );

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegisterPage);
