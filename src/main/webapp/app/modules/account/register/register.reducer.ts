import { ActionType, getType } from 'typesafe-actions';

import * as actions from './register.action';

type Action = ActionType<typeof actions>;

const initialState = {
  loading: false,
  registrationSuccess: false,
  registrationFailure: false,
  mnemonic: null,
  errorMessage: null
};

export type RegisterState = Readonly<typeof initialState>;

// Reducer
export const reducer = (state: RegisterState = initialState, action): RegisterState => {
  switch (action.type) {
    case getType(actions.createAccountAction):
      return {
        ...state,
        loading: true
      };
    case getType(actions.createAccountFailureAction):
      return {
        ...initialState,
        registrationFailure: true,
        errorMessage: action.payload.response.data.errorKey
      };
    case getType(actions.createAccountSuccessAction):
      return {
        ...initialState,
        registrationSuccess: true
      };
    case getType(actions.generateMnemonicAction):
      return {
        ...state,
        loading: true
      };
    case getType(actions.generateMnemonicSuccessAction):
      return {
        ...initialState,
        mnemonic: action.payload.mnemonic
      };
    case getType(actions.resetSuccessAction):
      return {
        ...initialState
      };
    default:
      return state;
  }
};
