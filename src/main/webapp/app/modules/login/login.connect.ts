import { ActionType } from 'typesafe-actions';
import { Dispatch, bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as actions from '../../shared/redux/actions';
import Login from './login';
import { IRootState } from 'app/shared/redux/reducers';
import { RouteComponentProps } from 'react-router-dom';

type Action = ActionType<typeof actions>;

export interface ILoginProps extends StateProps, DispatchProps, RouteComponentProps<{}> {}

export interface ILoginState {
  showModal: boolean;
}

const mapStateToProps = ({ authentication }: IRootState) => ({
  isAuthenticated: authentication.isAuthenticated,
  loginError: authentication.loginError,
  showModal: authentication.showModalLogin,
  login: (mnemonic, password) => undefined
});

const mapDispatchToProps = (dispatch: Dispatch<Action>, props: ILoginProps) =>
  bindActionCreators(
    {
      login: (mnemonic, password) => actions.loginAction(mnemonic, password)
    },
    dispatch
  );

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
