import { Dispatch, bindActionCreators } from 'redux';
import { ActionType } from 'typesafe-actions';

import { IRootState } from '../../shared/redux/reducers';
import * as actions from '../../shared/redux/actions';
import { connect } from 'react-redux';
import Logout from './logout';

type Action = ActionType<typeof actions>;
// import { logout } from 'app/shared/reducers/authentication';

export interface ILogoutProps extends StateProps, DispatchProps {
  logout: Function;
}

const mapStateToProps = (storeState: IRootState) => ({});

const mapDispatchToProps = (dispatch: Dispatch<Action>, props: ILogoutProps) =>
  bindActionCreators({ logout: () => actions.logoutAction() }, dispatch);

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Logout);
