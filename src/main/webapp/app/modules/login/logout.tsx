import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import { ILogoutProps } from './logout.connect';

export default class Logout extends React.Component<ILogoutProps> {
  componentDidMount() {
    this.props.logout();
  }

  render() {
    return (
      <div className="p-5">
        <h4>Logged out successfully!</h4>
        <Redirect
          to={{
            pathname: '/'
          }}
        />
      </div>
    );
  }
}
