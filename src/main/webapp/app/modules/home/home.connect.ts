import { ActionType } from 'typesafe-actions';
import { Dispatch, bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as actions from '../../shared/redux/actions';
import { Home } from './home';
import { IRootState } from 'app/shared/redux/reducers';

type Action = ActionType<typeof actions>;

export interface IHomeProps extends StateProps, DispatchProps {
  getSession: Function;
}

const mapStateToProps = ({ authentication }: IRootState) => ({
  account: authentication.account,
  isAuthenticated: authentication.isAuthenticated
});

const mapDispatchToProps = (dispatch: Dispatch<Action>, props: IHomeProps) =>
  bindActionCreators({ getSession: () => actions.getSessionAction() }, dispatch);

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
