import * as React from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import cellEditFactory from 'react-bootstrap-table2-editor';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Translate, translate } from 'react-jhipster';
import { Row, Col, Label, Button } from 'reactstrap';
import produce from 'immer';

import { IProfileProps } from './profile.connect';
import { Redirect } from 'react-router-dom';
import { AvForm, AvGroup, AvField } from 'availity-reactstrap-validation';

export interface IProfileState {
  profilesToUpdate: Map<string, string>;
}

export default class ProfilePage extends React.Component<IProfileProps, IProfileState> {
  state: Readonly<IProfileState> = {
    profilesToUpdate: new Map<string, string>()
  };

  columns = [
    {
      dataField: 'key',
      text: 'Key',
      sort: true
    },
    {
      dataField: 'value',
      text: 'Value'
    }
  ];

  cellEdit = cellEditFactory({
    mode: 'dbclick',
    blurToSave: true,
    nonEditableRows: () => ['key'],
    autoSelectText: true,
    afterSaveCell: (oldValue, newValue, row, column) => {
      if (oldValue !== newValue) {
        this.setState(
          produce<IProfileState>(draft => {
            const newProfilesToUpdate = new Map<string, string>(draft.profilesToUpdate);
            newProfilesToUpdate.set(row.key.toLowerCase(), row.value);
            draft.profilesToUpdate = newProfilesToUpdate;
          })
        );
      }
    }
  });

  defaultSorted = [
    {
      dataField: 'key',
      order: 'asc'
    }
  ];

  myFormRef: any;

  constructor(props: IProfileProps) {
    super(props);
    console.log('constructor');
  }

  componentWillMount() {
    console.log('componentWillMount');
  }

  componentDidMount() {
    console.log('componentDidMount');
    if (this.props.isAuthenticated) {
      this.props.loadProfile();
    }
  }

  shouldComponentUpdate() {
    console.log('shouldComponentUpdate');
    return true;
  }

  componentWillUpdate() {
    console.log('componentWillUpdate');
  }

  componentDidUpdate() {
    console.log('componentDidUpdate');
  }

  componentWillUnmount() {
    console.log('componentWillUnmount');
  }

  componentWillReceiveProps() {
    console.log('componentWillReceiveProps');
  }

  handleProfileAdd = (event, values) => {
    this.setState(
      produce<IProfileState>(draft => {
        const newProfilesToUpdate = new Map<string, string>(draft.profilesToUpdate);
        newProfilesToUpdate.set(values.key.toLowerCase(), values.value);
        draft.profilesToUpdate = newProfilesToUpdate;
      })
    );
    this.myFormRef.reset();
  };

  handleProfileUpdate = () => {
    this.props.updateProfile(this.state.profilesToUpdate);
    this.setState(
      produce<IProfileState>(draft => {
        draft.profilesToUpdate = new Map<string, string>();
      })
    );
  };

  public render() {
    const { account, isAuthenticated, profiles } = this.props;

    console.log('render');

    if (!isAuthenticated) {
      return <Redirect to="login" />;
    }

    const tableData = [];
    const profilesToDisplay = new Map<string, string>([
      ...Array.from(profiles.entries()),
      ...Array.from(this.state.profilesToUpdate.entries())
    ]);
    profilesToDisplay.forEach((v, k) => tableData.push({ key: k, value: v }));

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <Label>
              <Translate contentKey="profile.accountLabel">Account</Translate>:{' '}
              {account && account.publicKey && account.publicKey.length > 0 ? account.publicKey : ' '}
            </Label>
          </Col>
        </Row>
        <br />
        <Row className="justify-content-center" />
        <br />
        <Row className="justify-content-center">
          <Col md="8">
            <BootstrapTable
              keyField="key"
              data={tableData}
              columns={this.columns}
              cellEdit={this.cellEdit}
              defaultSorted={this.defaultSorted}
            />
            <Button
              color="primary"
              align="right"
              onClick={this.handleProfileUpdate}
              disabled={this.state.profilesToUpdate.size === 0}
            >
              <FontAwesomeIcon icon="save" />
              &nbsp;
              <Translate contentKey="profile.action.update">Update</Translate>
            </Button>
          </Col>
          <Col md="4">
            <AvForm onValidSubmit={this.handleProfileAdd} ref={el => (this.myFormRef = el)}>
              <AvGroup>
                <Label for="key">
                  <Translate contentKey="profile.form.label.key">Key</Translate>
                </Label>
                <AvField
                  type="text"
                  className="form-control"
                  name="key"
                  validate={{
                    required: {
                      value: true,
                      errorMessage: translate('profile.messages.validate.key.required')
                    },
                    minLength: {
                      value: 1,
                      errorMessage: translate('profile.messages.validate.key.minlength')
                    },
                    maxLength: {
                      value: 50,
                      errorMessage: translate('profile.messages.validate.key.maxlength')
                    }
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label for="value">
                  <Translate contentKey="profile.form.label.value">Value</Translate>
                </Label>
                <AvField
                  type="text"
                  className="form-control"
                  name="value"
                  validate={{
                    required: {
                      value: true,
                      errorMessage: translate('profile.messages.validate.value.required')
                    },
                    minLength: {
                      value: 1,
                      errorMessage: translate('profile.messages.validate.value.minlength')
                    },
                    maxLength: {
                      value: 50,
                      errorMessage: translate('profile.messages.validate.value.maxlength')
                    }
                  }}
                />
              </AvGroup>
              <Button color="primary" type="submit">
                <FontAwesomeIcon icon="plus" />
                &nbsp;
                <Translate contentKey="profile.form.submit">Add</Translate>
              </Button>
            </AvForm>
          </Col>
        </Row>
      </div>
    );
  }
}
