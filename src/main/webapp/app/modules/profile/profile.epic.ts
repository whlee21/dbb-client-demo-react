import { ActionType, isActionOf } from 'typesafe-actions';
import { Epic } from 'redux-observable';
import { of } from 'rxjs';
import { filter, map, catchError, switchMap } from 'rxjs/operators';
import { create } from 'rxjs-spy';

import * as actions from './profile.action';
import { ProfileState } from './profile.reducer';
import { dbbClientService } from 'app/shared/service/dbb-client.service';

const spy = create();

type Action = ActionType<typeof actions>;

const loadProfileEpic: Epic<Action, Action, ProfileState> = (action$, state$) =>
  action$.pipe(
    filter(isActionOf(actions.loadProfileAction)),
    switchMap(() => dbbClientService.getData()),
    map(data => actions.loadProfileSuccessAction(data)),
    catchError(error => of(actions.loadProfileFailureAction(error)))
  );

const updateProfileEpic: Epic<Action, Action, ProfileState> = (action$, state$) =>
  action$.pipe(
    filter(isActionOf(actions.updateProfileAction)),
    switchMap(action => dbbClientService.updateData(action.payload.profiles)),
    map(() => actions.updateProfileSuccessAction()),
    catchError(error => of(actions.updateProfileFailureAction(error)))
  );

const updateProfileSuccessEpic: Epic<Action, Action, ProfileState> = (action$, state$) =>
  action$.pipe(
    filter(isActionOf(actions.updateProfileSuccessAction)),
    map(() => actions.loadProfileAction())
  );

export default [loadProfileEpic, updateProfileEpic, updateProfileSuccessEpic];
