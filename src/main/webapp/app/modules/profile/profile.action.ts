import { createAction } from 'typesafe-actions';

const LOAD_PROFILE = 'profile/LOAD';
const LOAD_PROFILE_SUCCESS = 'profile/LOAD_SUCCESS';
const LOAD_PROFILE_FAILURE = 'profile/LOAD_FAILURE';

const UPDATE_PROFILE = 'profile/UPDATE';
const UPDATE_PROFILE_SUCCESS = 'profile/UPDATE_SUCCESS';
const UPDATE_PROFILE_FAILURE = 'profile/UPDATE_FAILURE';

export const loadProfileAction = createAction(LOAD_PROFILE);
export const loadProfileSuccessAction = createAction(LOAD_PROFILE_SUCCESS, resolve => (profiles: Map<string, string>) =>
  resolve({ profiles })
);
export const loadProfileFailureAction = createAction(LOAD_PROFILE_FAILURE, resolve => (error: Error) =>
  resolve({ error })
);

export const updateProfileAction = createAction(UPDATE_PROFILE, resolve => (profiles: Map<string, string>) =>
  resolve({ profiles })
);
export const updateProfileSuccessAction = createAction(UPDATE_PROFILE_SUCCESS);
export const updateProfileFailureAction = createAction(UPDATE_PROFILE_FAILURE, resolve => (error: Error) =>
  resolve({ error })
);
