import { ActionType, getType } from 'typesafe-actions';
import produce, { Draft } from 'immer';

import * as actions from './profile.action';

type Action = ActionType<typeof actions>;

const initialState = {
  loading: false,
  profiles: new Map<string, string>(),
  error: ''
};

export type ProfileState = Readonly<typeof initialState>;

export const reducer = (state: ProfileState = initialState, action: Action): ProfileState => {
  switch (action.type) {
    case getType(actions.loadProfileAction):
      return produce<ProfileState>(state, draft => {
        draft.loading = true;
      });
    case getType(actions.updateProfileAction):
      return produce<ProfileState>(state, draft => {
        draft.loading = true;
        draft.profiles = action.payload.profiles;
      });
    case getType(actions.loadProfileSuccessAction):
      return produce<ProfileState>(initialState, draft => {
        draft.profiles = action.payload.profiles;
      });
    case getType(actions.updateProfileSuccessAction):
      return produce<ProfileState>(state, draft => {
        draft.loading = false;
      });
    case getType(actions.loadProfileFailureAction):
      return produce<ProfileState>(initialState, draft => {
        draft.error = action.payload.error.message;
      });
    default:
      return state;
  }
};
