import { ActionType } from 'typesafe-actions';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';

import * as actions from './profile.action';
import { IRootState } from 'app/shared/redux/reducers';
import ProfilePage from './profile';

type Action = ActionType<typeof actions>;

export interface IProfileProps extends StateProps, DispatchProps, RouteComponentProps<{}> {
  loadProfile: Function;
  updateProfile: (profiles: Map<string, string>) => undefined;
  addProfile: (key: string, value: string) => undefined;
  updateOneProfile: (key: string, value: string) => undefined;
}

const mapStateToProps = ({ authentication, profile }: IRootState) => ({
  account: authentication.account,
  isAuthenticated: authentication.isAuthenticated,
  profiles: profile.profiles
});

const mapDispatchToProps = (dispatch: Dispatch<Action>, props: IProfileProps) =>
  bindActionCreators(
    {
      loadProfile: () => actions.loadProfileAction(),
      updateProfile: (profiles: Map<string, string>) => actions.updateProfileAction(profiles)
    },
    dispatch
  );

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfilePage);
