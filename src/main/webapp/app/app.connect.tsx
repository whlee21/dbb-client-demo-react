// import { getSession } from './shared/reducers/authentication';
import { Dispatch, bindActionCreators } from 'redux';
import { ActionType } from 'typesafe-actions';
import { connect } from 'react-redux';

import { IRootState } from './shared/redux/reducers';
import * as actions from './shared/redux/actions';
import App from './app';

type Action = ActionType<typeof actions>;

export interface IAppProps extends StateProps, DispatchProps {
  getSession: Function;
  changeLocale: (locale: string) => undefined;
}

const mapStateToProps = ({ authentication, locale }: IRootState) => ({
  isAuthenticated: authentication.isAuthenticated,
  currentLocale: locale.currentLocale
});

const mapDispatchToProps = (dispatch: Dispatch<Action>, props: IAppProps) =>
  bindActionCreators(
    {
      getSession: () => actions.getSessionAction(),
      changeLocale: locale => actions.setLocaleAction(locale)
    },
    dispatch
  );

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
