import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { AppContainer } from 'react-hot-loader';

import initStore from './config/store';
import ErrorBoundary from './shared/error/error-boundary';
import AppComponent from './app.connect';
import { loadIcons } from './config/icon-loader';
import { registerLocale } from './config/translation';

const store = initStore();
registerLocale(store);

loadIcons();

const rootEl = document.getElementById('root');

const render = Component =>
  ReactDOM.render(
    <ErrorBoundary>
      <AppContainer>
        <Provider store={store}>
          <div>
            {/* If this slows down the app in dev disable it and enable when required  */}
            <Component />
          </div>
        </Provider>
      </AppContainer>
    </ErrorBoundary>,
    rootEl
  );

render(AppComponent);
