import React from 'react';
import { Switch } from 'react-router-dom';
import Loadable from 'react-loadable';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import Login from './modules/login/login.connect';
import Logout from './modules/login/logout.connect';
import Home from './modules/home/home.connect';
import Register from './modules/account/register/register.connect';
// import PrivateRoute from './shared/auth/private-route.connect';
import Profile from './modules/profile/profile.connect';

// tslint:disable:space-in-parens
// const Profile = Loadable({
//   loader: () => import(/* webpackChunkName: "profile" */ 'app/modules/profile'),
//   loading: () => <div>loading ...</div>
// });
// tslint:enable

const Routes = () => (
  <div className="view-routes">
    <ErrorBoundaryRoute path="/login" component={Login} />
    <Switch>
      <ErrorBoundaryRoute path="/logout" component={Logout} />
      <ErrorBoundaryRoute path="/register" component={Register} />
      <ErrorBoundaryRoute path="/profile" component={Profile} />
      <ErrorBoundaryRoute path="/" component={Home} />
    </Switch>
  </div>
);

export default Routes;
