import './app.scss';

import * as React from 'react';
import { Card } from 'reactstrap';
import { HashRouter as Router } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';

import ErrorBoundary from './shared/error/error-boundary';
import Header from './shared/layout/header/header';
import Footer from './shared/layout/footer/footer';
import AppRoutes from 'app/routes';

import { IAppProps } from './app.connect';

export default class App extends React.Component<IAppProps> {
  componentDidMount() {
    this.props.getSession();
  }

  render() {
    const paddingTop = '60px';
    return (
      <Router>
        <div className="app-container" style={{ paddingTop }}>
          <ToastContainer
            position={toast.POSITION.TOP_LEFT}
            className="toastify-container"
            toastClassName="toastify-toast"
          />
          <ErrorBoundary>
            <Header
              isAuthenticated={this.props.isAuthenticated}
              currentLocale={this.props.currentLocale}
              onLocaleChange={this.props.changeLocale}
            />
          </ErrorBoundary>
          <div className="container-fluid view-container" id="app-view-container">
            <Card className="jh-card">
              <ErrorBoundary>
                <AppRoutes />
              </ErrorBoundary>
            </Card>
            <Footer />
          </div>
        </div>
      </Router>
    );
  }
}
