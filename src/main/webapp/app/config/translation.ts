import { TranslatorContext, Storage } from 'react-jhipster';

import * as actions from '../shared/redux/actions';

TranslatorContext.setDefaultLocale('ko');
TranslatorContext.setRenderInnerTextForMissingKeys(false);

export const languages: any = {
  en: { name: 'English' },
  ko: { name: '한국어' }
  // jhipster-needle-i18n-language-key-pipe - JHipster will add/remove languages in this object
};

export const locales = Object.keys(languages).sort();

export const registerLocale = store => {
  store.dispatch(actions.setLocaleAction(Storage.session.get('locale', 'ko')));
};
