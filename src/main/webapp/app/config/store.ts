import { createStore, applyMiddleware, compose } from 'redux';
import { createEpicMiddleware } from 'redux-observable';
import { ActionType } from 'typesafe-actions';
import reducer, { IRootState } from '../shared/redux/reducers';
import errorMiddleware from './error-middleware';
import notificationMiddleware from './notification-middleware';
import loggerMiddleware from './logger-middleware';

import epics from '../shared/redux/epics';
import * as actions from '../shared/redux/actions';

declare let window: any;

type Action = ActionType<typeof actions>;

export const epicMiddleware = createEpicMiddleware<Action, Action, IRootState>();

const defaultMiddlewares = [epicMiddleware, errorMiddleware, notificationMiddleware, loggerMiddleware];

/* eslint-disable no-underscore-dangle */
const composeEnhancers =
  process.env.NODE_ENV === 'development' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        trace: true,
        traceLimit: 25
      })
    : compose;
/* eslint-enable */

const enhancer = middlewares => composeEnhancers(applyMiddleware(...defaultMiddlewares, ...middlewares));

const initialize = (initialState?: IRootState, middlewares = []) => {
  const store = createStore(reducer, initialState, enhancer(middlewares));

  epicMiddleware.run(epics);
  return store;
};

export default initialize;
