import { ajax } from 'rxjs/ajax';

// const getSession = () => ajax('api/account').subscribe(res => res.response);
export const getSession = () => ajax.get('api/account');
// await dispatch({
//   type: GET_SESSION,
//   payload: axios.get('api/account')
// });
// fetch('api/account').then(response => response.json());
// new Promise<any>(resolve => resolve());

// const { account } = getState().authentication;
// if (account && account.langKey) {
//   const langKey = Storage.session.get('locale', account.langKey);
//   await dispatch(setLocale(langKey));
// }

export const login = (username, password, rememberMe = false) => ajax.get('api/authenticate');

// export const login = (username, password, rememberMe = false) => async (dispatch, getState) => {
//   const result = await dispatch({
//     type: LOGIN,
//     payload: axios.post('api/authenticate', { username, password, rememberMe })
//   });
//   const bearerToken = result.value.headers.authorization;
//   if (bearerToken && bearerToken.slice(0, 7) === 'Bearer ') {
//     const jwt = bearerToken.slice(7, bearerToken.length);
//     if (rememberMe) {
//       Storage.local.set(AUTH_TOKEN_KEY, jwt);
//     } else {
//       Storage.session.set(AUTH_TOKEN_KEY, jwt);
//     }
//   }
//   await dispatch(getSession());
// };

// export const clearAuthToken = () => {
//   if (Storage.local.get(AUTH_TOKEN_KEY)) {
//     Storage.local.remove(AUTH_TOKEN_KEY);
//   }
//   if (Storage.session.get(AUTH_TOKEN_KEY)) {
//     Storage.session.remove(AUTH_TOKEN_KEY);
//   }
// };

// export const logout = () => dispatch => {
//   clearAuthToken();
//   dispatch({
//     type: LOGOUT
//   });
// };

// export const clearAuthentication = messageKey => (dispatch, getState) => {
//   clearAuthToken();
//   dispatch(displayAuthError(messageKey));
//   dispatch({
//     type: CLEAR_AUTH
//   });
// };
