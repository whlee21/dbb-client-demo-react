import { ajax } from 'rxjs/ajax';

export const getLocale = (locale: string) =>
  fetch(`i18n/${locale}.json?buildTimestamp=${process.env.BUILD_TIMESTAMP}`).then(res => res.json());
