import { TranslatorContext } from 'react-jhipster';
import { ActionType, getType } from 'typesafe-actions';
import * as actions from '../actions';

type Action = ActionType<typeof actions>;

const initialState = {
  currentLocale: undefined
};

export type LocaleState = Readonly<typeof initialState>;

export const reducer = (state: LocaleState = initialState, action: Action): LocaleState => {
  switch (action.type) {
    case getType(actions.setCurrentLocaleAction):
      const currentLocale = action.payload.locale;
      if (state.currentLocale !== currentLocale) {
        TranslatorContext.setLocale(currentLocale);
      }
      return {
        currentLocale
      };
    default:
      return state;
  }
};
