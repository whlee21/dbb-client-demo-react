import { combineReducers } from 'redux';
import { loadingBarReducer } from 'react-redux-loading-bar';

import { reducer as authReducer, AuthenticationState } from './authentication';
import { reducer as localeReducer, LocaleState } from './locale';
import { reducer as registerReducer, RegisterState } from 'app/modules/account/register';

/* jhipster-needle-add-reducer-import - JHipster will add reducer here */
import { reducer as profileReducer, ProfileState } from 'app/modules/profile';

export interface IRootState {
  readonly authentication: AuthenticationState;
  readonly locale: LocaleState;
  readonly register: RegisterState;
  readonly profile: ProfileState;
  readonly loadingBar: any;
}

const rootReducer = combineReducers<IRootState>({
  authentication: authReducer,
  locale: localeReducer,
  register: registerReducer,
  profile: profileReducer,
  loadingBar: loadingBarReducer
});

export default rootReducer;
