import { ActionType, getType } from 'typesafe-actions';
import { Account } from 'dbb-client-js';

import * as actions from '../actions';

type Action = ActionType<typeof actions>;

const initialState = {
  loading: false,
  isAuthenticated: false,
  loginSuccess: false,
  loginError: false, // Errors returned from server side
  showModalLogin: false,
  account: null as Account,
  errorMessage: null as string, // Errors returned from server side
  redirectMessage: null as string,
  sessionHasBeenFetched: false
};

export type AuthenticationState = Readonly<typeof initialState>;

// Reducer
export const reducer = (state: AuthenticationState = initialState, action: Action): AuthenticationState => {
  switch (action.type) {
    case getType(actions.loginAction):
    case getType(actions.getSessionAction):
      return {
        ...state,
        loading: true
      };
    case getType(actions.loginFailureAction):
      return {
        ...initialState,
        errorMessage: action.payload.error.message,
        showModalLogin: true,
        loginError: true
      };
    case getType(actions.getSessionFailureAction):
      return {
        ...state,
        loading: false,
        isAuthenticated: false,
        sessionHasBeenFetched: true,
        showModalLogin: true,
        errorMessage: action.payload.error.message
      };
    case getType(actions.loginSuccessAction):
      return {
        ...state,
        loading: false,
        account: action.payload.account,
        loginError: false,
        showModalLogin: false,
        loginSuccess: true
      };
    case getType(actions.logoutSuccessAction):
      return {
        ...initialState,
        showModalLogin: true
      };
    case getType(actions.getSessionSuccessAction): {
      const isAuthenticated = action.payload && action.payload.account && action.payload.account.publicKey.length > 0;
      return {
        ...state,
        isAuthenticated,
        loading: false,
        sessionHasBeenFetched: true,
        account: action.payload.account
      };
    }
    case getType(actions.errorMessageAction):
      return {
        ...initialState,
        showModalLogin: true,
        redirectMessage: action.payload.error.message
      };
    case getType(actions.clearAuthAction):
      return {
        ...state,
        loading: false,
        showModalLogin: true,
        isAuthenticated: false
      };
    default:
      return state;
  }
};

// export const displayAuthError = message => ({ type: ERROR_MESSAGE, message });

// export const getSession = () => async (dispatch, getState) => {
//   await dispatch({
//     type: GET_SESSION,
//     payload: axios.get('api/account')
//   });

//   const { account } = getState().authentication;
//   if (account && account.langKey) {
//     const langKey = Storage.session.get('locale', account.langKey);
//     await dispatch(setLocale(langKey));
//   }
// };

// export const login = (username, password, rememberMe = false) => async (dispatch, getState) => {
//   const result = await dispatch({
//     type: LOGIN,
//     payload: axios.post('api/authenticate', { username, password, rememberMe })
//   });
//   const bearerToken = result.value.headers.authorization;
//   if (bearerToken && bearerToken.slice(0, 7) === 'Bearer ') {
//     const jwt = bearerToken.slice(7, bearerToken.length);
//     if (rememberMe) {
//       Storage.local.set(AUTH_TOKEN_KEY, jwt);
//     } else {
//       Storage.session.set(AUTH_TOKEN_KEY, jwt);
//     }
//   }
//   await dispatch(getSession());
// };

// export const clearAuthToken = () => {
//   if (Storage.local.get(AUTH_TOKEN_KEY)) {
//     Storage.local.remove(AUTH_TOKEN_KEY);
//   }
//   if (Storage.session.get(AUTH_TOKEN_KEY)) {
//     Storage.session.remove(AUTH_TOKEN_KEY);
//   }
// };

// export const logout = () => dispatch => {
//   clearAuthToken();
//   dispatch({
//     type: LOGOUT
//   });
// };

// export const clearAuthentication = messageKey => (dispatch, getState) => {
//   clearAuthToken();
//   dispatch(displayAuthError(messageKey));
//   dispatch({
//     type: CLEAR_AUTH
//   });
// };
