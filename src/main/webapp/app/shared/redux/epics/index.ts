import { combineEpics } from 'redux-observable';
import localeEpic from './locale';
import authEpic from './authentication';
import registerEpic from 'app/modules/account/register/register.epic';
import profileEpic from 'app/modules/profile/profile.epic';

const epics = combineEpics(...localeEpic, ...authEpic, ...registerEpic, ...profileEpic);

export default epics;
