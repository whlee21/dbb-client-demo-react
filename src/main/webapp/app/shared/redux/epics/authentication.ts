import { of, iif } from 'rxjs';
import { mergeMap, map, catchError, filter, switchMap } from 'rxjs/operators';
import { Epic } from 'redux-observable';
import { ActionType, isActionOf } from 'typesafe-actions';
import { Storage } from 'react-jhipster';

import * as actions from '../actions/authentication';
import { AuthenticationState } from '../reducers/authentication';
import { dbbClientService } from '../../service/dbb-client.service';
import { unregister } from '../../../registerServiceWorker';

const AUTH_TOKEN_KEY = 'jhi-authenticationToken';

type Action = ActionType<typeof actions>;

const getSessionEpic: Epic<Action, Action, AuthenticationState> = (action$, state$) =>
  action$.pipe(
    filter(isActionOf(actions.getSessionAction)),
    mergeMap(() =>
      iif(
        () => dbbClientService.getAccount().publicKey.length > 0,
        of(actions.getSessionSuccessAction(dbbClientService.getAccount())),
        of(actions.getSessionFailureAction(new Error('Session not found')))
      )
    )
  );

const loginEpic: Epic<Action, Action, AuthenticationState> = (action$, state$) =>
  action$.pipe(
    filter(isActionOf(actions.loginAction)),
    mergeMap(action =>
      dbbClientService.checkAccount(action.payload.mnemonic, action.payload.password).pipe(
        map(account => actions.loginSuccessAction(account)),
        catchError(error => of(actions.loginFailureAction(error)))
      )
    )
  );

const loginSuccessEpic: Epic<Action, Action, AuthenticationState> = (action$, state$) =>
  action$.pipe(
    filter(isActionOf(actions.loginSuccessAction)),
    map(() => actions.getSessionAction())
  );

const logoutEpic: Epic<Action, Action, AuthenticationState> = (action$, state$) =>
  action$.pipe(
    filter(isActionOf(actions.logoutAction)),
    map(action => {
      dbbClientService.clearAccount();
      return actions.logoutSuccessAction();
    })
  );

export default [getSessionEpic, loginEpic, loginSuccessEpic, logoutEpic];

export const clearAuthToken = () => {
  if (Storage.local.get(AUTH_TOKEN_KEY)) {
    Storage.local.remove(AUTH_TOKEN_KEY);
  }
  if (Storage.session.get(AUTH_TOKEN_KEY)) {
    Storage.session.remove(AUTH_TOKEN_KEY);
  }
};
