import { of, from, iif } from 'rxjs';
import { filter, map, mergeMap } from 'rxjs/operators';
import { Epic } from 'redux-observable';
import { ActionType, isActionOf } from 'typesafe-actions';

import { LocaleState } from '../reducers/locale';
import { getLocale } from '../../apis';
import * as actions from '../actions/locale';
import { TranslatorContext } from 'react-jhipster';

type Action = ActionType<typeof actions>;

const setLocaleEpic: Epic<Action, Action, LocaleState> = (action$, state$) =>
  action$.pipe(
    filter(isActionOf(actions.setLocaleAction)),
    mergeMap(action =>
      iif(
        () => !Object.keys(TranslatorContext.context.translations).includes(action.payload.locale),
        from(getLocale(action.payload.locale)).pipe(
          map(translation => TranslatorContext.registerTranslations(action.payload.locale, translation)),
          map(() => action)
        ),
        of(action)
      )
    ),
    map(action => actions.setCurrentLocaleAction(action.payload.locale))
  );

export default [setLocaleEpic];
