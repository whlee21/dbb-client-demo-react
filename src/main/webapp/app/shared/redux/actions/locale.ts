import { createAction } from 'typesafe-actions';

const SET_LOCALE = 'locale/SET_LOCALE';
const SET_CURRENT_LOCALE = 'locale/SET_CURRENT_LOCALE';

export const setLocaleAction = createAction(SET_LOCALE, resolve => (locale: string) => resolve({ locale }));

export const setCurrentLocaleAction = createAction(SET_CURRENT_LOCALE, resolve => (locale: string) =>
  resolve({ locale })
);
