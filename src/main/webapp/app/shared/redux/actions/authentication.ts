import { Account } from 'dbb-client-js';

import { createAction } from 'typesafe-actions';

const LOGIN = 'authentication/LOGIN';
const LOGIN_SUCCESS = 'authentication/LOGIN_SUCCESS';
const LOGIN_FAILURE = 'authentication/LOGIN_FAILURE';

const GET_SESSION = 'authentication/GET_SESSION';
const GET_SESSION_SUCCESS = 'authentication/GET_SESSION_SUCCESS';
const GET_SESSION_FAILURE = 'authentication/GET_SESSION_FAILURE';

const LOGOUT = 'authentication/LOGOUT';
const LOGOUT_SUCCESS = 'authentication/LOGOUT_SUCCESS';
const LOGOUT_FAILURE = 'authentication/LOGOUT_FAILURE';

const CLEAR_AUTH = 'authentication/CLEAR_AUTH';
const ERROR_MESSAGE = 'authentication/ERROR_MESSAGE';

export const loginAction = createAction(LOGIN, resolve => (mnemonic: string, password: string) =>
  resolve({ mnemonic, password })
);
export const loginSuccessAction = createAction(LOGIN_SUCCESS, resolve => (account: Account) => resolve({ account }));
export const loginFailureAction = createAction(LOGIN_FAILURE, resolve => (error: Error) => resolve({ error }));

export const getSessionAction = createAction(GET_SESSION);
export const getSessionSuccessAction = createAction(GET_SESSION_SUCCESS, resolve => (account: Account) =>
  resolve({ account })
);
export const getSessionFailureAction = createAction(GET_SESSION_FAILURE, resolve => (error: Error) =>
  resolve({ error })
);

export const logoutAction = createAction(LOGOUT);
export const logoutSuccessAction = createAction(LOGOUT_SUCCESS);
export const logoutFailureAction = createAction(LOGOUT_FAILURE, resolve => (error: Error) => resolve({ error }));

export const clearAuthAction = createAction(CLEAR_AUTH);
export const errorMessageAction = createAction(ERROR_MESSAGE, resolve => (error: Error) => resolve({ error }));
