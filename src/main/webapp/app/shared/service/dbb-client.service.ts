import { Observable, of } from 'rxjs';
import { DbbClient, Account } from 'dbb-client-js';
import { switchMap } from 'rxjs/operators';

class DbbClientService {
  private dbbClient: DbbClient;

  constructor() {
    this.dbbClient = new DbbClient('http://localhost:8080', 'localhost');
  }

  getNewMnemonic(): Observable<string> {
    return this.dbbClient.accountManager.getNewMnemonic();
  }

  registration(mnemonic: string, message: string): Observable<Account> {
    return this.dbbClient.accountManager.registration(mnemonic, message);
  }

  checkAccount(mnemonic: string, message: string): Observable<Account> {
    return this.dbbClient.accountManager.checkAccount(mnemonic, message);
  }

  getAccount(): Account {
    return this.dbbClient.accountManager.getAccount();
  }

  clearAccount(): void {
    return this.dbbClient.accountManager.clearAccount();
  }

  unsubscribe(): Observable<Account> {
    return this.dbbClient.accountManager.unsubscribe();
  }

  getData(): Observable<Map<string, string>> {
    return this.dbbClient.profileManager.getData();
  }

  getRawData(anyPublicKey: string): Observable<Map<string, string>> {
    return this.dbbClient.profileManager.getRawData(anyPublicKey);
  }

  getAuthorizedData(recipientPk: string, encryptedData: string): Observable<Map<string, string>> {
    return this.dbbClient.profileManager.getAuthorizedData(recipientPk, encryptedData);
  }

  getAuthorizedEncryptionKeys(recipientPk: string, encryptedData: string): Observable<Map<string, string>> {
    return this.dbbClient.profileManager.getAuthorizedEncryptionKeys(recipientPk, encryptedData);
  }

  updateData(data: Map<string, string>): Observable<Map<string, string>> {
    return this.dbbClient.profileManager.updateData(data);
  }
}

export const dbbClientService = new DbbClientService();
